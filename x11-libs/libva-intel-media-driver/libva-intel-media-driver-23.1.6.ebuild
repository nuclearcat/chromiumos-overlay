# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

CROS_WORKON_COMMIT="8589406d4e7f97c6de55862af775d67d5dd7cd28"
CROS_WORKON_TREE="eba8116ae241de5ba9a6c73f2a2658b71f542c06"
CROS_WORKON_PROJECT="chromiumos/third_party/libva-intel-media-driver"
CROS_WORKON_MANUAL_UPREV="1"
CROS_WORKON_LOCALNAME="libva-intel-media-driver"
CROS_WORKON_EGIT_BRANCH="chromeos"

inherit cmake cros-workon

KEYWORDS="*"
DESCRIPTION="Intel Media Driver for VAAPI (iHD)"
HOMEPAGE="https://github.com/intel/media-driver"

LICENSE="MIT BSD"
SLOT="0"
IUSE="ihd_cmrtlib video_cards_iHD_g8 video_cards_iHD_g9 video_cards_iHD_g11 video_cards_iHD_g12"
REQUIRED_USE="|| ( video_cards_iHD_g8 video_cards_iHD_g9 video_cards_iHD_g11 video_cards_iHD_g12 )"

DEPEND=">=media-libs/gmmlib-22.0.0:=
	>=x11-libs/libva-2.14.0
"
RDEPEND="${DEPEND}"

PATCHES=(
	"${FILESDIR}"/${PN}-21.4.2-Remove-unwanted-CFLAGS.patch
	"${FILESDIR}"/${PN}-20.4.5_testing_in_src_test.patch

	"${FILESDIR}"/0001-Disable-IPC-usage.patch
	"${FILESDIR}"/0002-change-slice-header-prefix-for-AVC-Vdenc.patch
	"${FILESDIR}"/0003-Disable-Media-Memory-Compression-MMC-on-ADL.patch
	"${FILESDIR}"/0004-VP9-Encode-Fix-unaligned-height-static-content-encod.patch
	"${FILESDIR}"/0005-Media-Common-VP-Update-Modifier-code-logic.patch
	"${FILESDIR}"/0006-Add-WaDisableGmmLibOffsetInDeriveImage-WA-on-gen8-9-.patch
	"${FILESDIR}"/0007-Handle-odd-dimensions-for-external-non-compressible-.patch
	"${FILESDIR}"/0008-VP9-Encode-Do-not-fill-padding-to-recon-surface.patch
	"${FILESDIR}"/0009-VP9-Encode-Fill-padding-to-tiled-format-buffer-direc.patch
	"${FILESDIR}"/0010-Remove-WaDisableGmmLibOffsetInDeriveImage-WA-for-APL.patch
	"${FILESDIR}"/0011-Media-Common-VP-fix-compressed-surface-width-not-ali.patch
	"${FILESDIR}"/0012-Encode-Add-data-for-back-annotation-in-status-report.patch
)

src_configure() {
	cros_optimize_package_for_speed
	local mycmakeargs=(
		-DMEDIA_RUN_TEST_SUITE=OFF
		-DBUILD_TYPE=Release
		-DPLATFORM=linux
		-DCMAKE_DISABLE_FIND_PACKAGE_X11=TRUE
		-DBUILD_CMRTLIB=$(usex ihd_cmrtlib ON OFF)

		-DGEN8=$(usex video_cards_iHD_g8 ON OFF)
		-DGEN9=$(usex video_cards_iHD_g9 ON OFF)
		-DGEN10=OFF
		-DGEN11=$(usex video_cards_iHD_g11 ON OFF)
		-DGEN12=$(usex video_cards_iHD_g12 ON OFF)
	)
	local CMAKE_BUILD_TYPE="Release"
	cmake_src_configure
}
