# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

CMAKE_BUILD_TYPE="Release"

CROS_WORKON_COMMIT="1f4fe32455e7f0d8e4d10bc3a51c5e64dd60a4ec"
CROS_WORKON_TREE="98042ed75259ec67cf8dd6f95e01038dbffa2063"
CROS_WORKON_PROJECT="chromiumos/third_party/gmmlib"
CROS_WORKON_MANUAL_UPREV="1"
CROS_WORKON_LOCALNAME="gmmlib"
CROS_WORKON_EGIT_BRANCH="chromeos"

inherit cmake cros-workon

DESCRIPTION="Intel Graphics Memory Management Library"
HOMEPAGE="https://github.com/intel/gmmlib"

KEYWORDS="*"
LICENSE="MIT"
SLOT="0/12.1"
IUSE="+custom-cflags test"
RESTRICT="!test? ( test )"

PATCHES=(
	"${FILESDIR}"/${PN}-20.2.2_conditional_testing.patch
	"${FILESDIR}"/${PN}-20.3.2_cmake_project.patch
	"${FILESDIR}"/${PN}-22.1.1_custom_cflags.patch
)

src_configure() {
	local mycmakeargs=(
		-DBUILD_TESTING="$(usex test)"
		-DBUILD_TYPE="Release"
		-DOVERRIDE_COMPILER_FLAGS="$(usex !custom-cflags)"
	)

	cmake_src_configure
}
