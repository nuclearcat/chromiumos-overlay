# Copyright 2022 The ChromiumOS Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

CROS_WORKON_COMMIT="5549f72e66fc69d406b681111c23ebccf51b6f18"
CROS_WORKON_TREE=("f91b6afd5f2ae04ee9a2c19109a3a4a36f7659e6" "d5188fec8e14c2a9a2c16c9645dc83908534149c" "b436b2188c971c497008cf6cfcc56b0b718d924c" "83b5dd27984d304a08a3043b55c2708afe90522e" "f12a2f998297133c0e97f61e1fa8f901e7f6852b" "6004a0e1699013ec8cf7e1061b6ed785e492f1a1" "7e22014f07fcc3faa5fb98a17f81dc91b4ca47ba")
CROS_WORKON_PROJECT="chromiumos/platform2"
CROS_WORKON_LOCALNAME="../platform2"
# TODO(b/187784160): Avoid directly including headers from other packages.
CROS_WORKON_SUBTREE=".gn camera/build camera/common camera/hal/fake camera/include camera/mojo common-mk"
CROS_WORKON_OUTOFTREE_BUILD="1"
CROS_WORKON_INCREMENTAL_BUILD="1"

PLATFORM_SUBDIR="camera/hal/fake"

inherit cros-camera cros-workon platform

DESCRIPTION="ChromeOS fake camera HAL."

LICENSE="BSD-Google"
KEYWORDS="*"
IUSE=""

RDEPEND="
	chromeos-base/cros-camera-android-deps:=
	media-libs/libsync:=
	media-libs/libyuv:="
DEPEND="${RDEPEND}"
BDEPEND="virtual/pkgconfig:="
