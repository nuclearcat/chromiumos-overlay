# Copyright 2021 The ChromiumOS Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

CROS_WORKON_COMMIT="b880a2ab2a91cc507f5b22e5d9c533d1d42da8f0"
CROS_WORKON_TREE="303214fff73c6cf22ab4f3ae0ebee94770c60709"
CROS_WORKON_PROJECT="chromiumos/third_party/libcamera"

inherit cros-workon

DESCRIPTION="Camera support library for Linux virtual package"

LICENSE="LGPL-2.1+"
SLOT="0"
KEYWORDS="*"

RDEPEND="
	media-libs/libcamera-upstream
"
