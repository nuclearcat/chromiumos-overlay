# Copyright 2021 The ChromiumOS Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

CROS_WORKON_PROJECT="chromiumos/third_party/libcamera"

inherit cros-workon

DESCRIPTION="Camera support library for Linux virtual package"

LICENSE="LGPL-2.1+"
SLOT="0"
KEYWORDS="~*"

RDEPEND="
	media-libs/libcamera-upstream
"
