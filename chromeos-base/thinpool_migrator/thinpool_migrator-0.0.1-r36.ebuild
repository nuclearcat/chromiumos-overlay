# Copyright 2023 The ChromiumOS Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

CROS_WORKON_COMMIT="5a280f82f28cfde70cf96e943da821469d4d2639"
CROS_WORKON_TREE=("7e22014f07fcc3faa5fb98a17f81dc91b4ca47ba" "1f8e0d50ae6bb0fb892d2776e8135efc59e5108a" "01667b873a2397868c4b80d4fe1b4be7b5d04fca" "f91b6afd5f2ae04ee9a2c19109a3a4a36f7659e6")
CROS_WORKON_INCREMENTAL_BUILD=1
CROS_WORKON_LOCALNAME="platform2"
CROS_WORKON_PROJECT="chromiumos/platform2"
CROS_WORKON_OUTOFTREE_BUILD=1
CROS_WORKON_SUBTREE="common-mk metrics thinpool_migrator .gn"

PLATFORM_SUBDIR="thinpool_migrator"

inherit cros-workon platform

DESCRIPTION="Thinpool migrator for ChromiumOS"
HOMEPAGE="https://chromium.googlesource.com/chromiumos/platform2/+/HEAD/thinpool_migrator/"

LICENSE="BSD-Google"
KEYWORDS="*"
IUSE="+device_mapper -lvm_stateful_partition"

COMMON_DEPEND="
	chromeos-base/metrics:=
	sys-apps/rootdev:=
	device_mapper? ( sys-fs/lvm2:=[thin] )
	lvm_stateful_partition? ( sys-fs/lvm2:= )
"
RDEPEND="
	${COMMON_DEPEND}
"
DEPEND="
	${COMMON_DEPEND}
"

platform_pkg_test() {
	platform test_all
}
