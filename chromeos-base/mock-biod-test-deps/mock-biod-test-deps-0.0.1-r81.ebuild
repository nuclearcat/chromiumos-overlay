# Copyright 2023 The ChromiumOS Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

EAPI=7

CROS_WORKON_COMMIT="46738754d2e7ae43aa25978437e42443f75896ff"
CROS_WORKON_TREE=("7e22014f07fcc3faa5fb98a17f81dc91b4ca47ba" "8cd9f5444a39d820d4ae97dc85addf456154a50f" "f91b6afd5f2ae04ee9a2c19109a3a4a36f7659e6")
CROS_WORKON_INCREMENTAL_BUILD=1
CROS_WORKON_LOCALNAME="platform2"
CROS_WORKON_PROJECT="chromiumos/platform2"
CROS_WORKON_OUTOFTREE_BUILD=1
CROS_WORKON_SUBTREE="common-mk biod .gn"

PLATFORM_SUBDIR="biod/mock-biod-test-deps"

inherit cros-workon platform

DESCRIPTION="biod test-only dbus policies. This package resides in test image only."
HOMEPAGE="https://chromium.googlesource.com/chromiumos/platform2/+/HEAD/biod/"

LICENSE="BSD-Google"
KEYWORDS="*"

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""

src_compile() {
	# We only install policy files here, no need to compile.
	:
}

src_install() {
	platform_src_install
}
