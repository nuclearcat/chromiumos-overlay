#!/bin/sh
# Copyright 2023 The ChromiumOS Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
#
# This script sets up Cr50 flash log time base, and then retrieves the Cr50
# flash log entries collected since the previous run and reports flash log
# events IDs to the UMA server.
#
# The previous run is identified by the timestamp saved in a file which
# survives reboots, updates and powerwashes.
#
# if the file does not exist, all Cr50 flash log entries are extracted and the
# timestamp of the last entry is saved.

/usr/share/cros/hwsec-utils/cr50_flash_log "$@"
