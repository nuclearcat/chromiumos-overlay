#!/bin/sh
# Copyright 2023 The ChromiumOS Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
#
# Reads board id space and format it as "XXXXXXXX:XXXXXXXX:XXXXXXXX" in hex.
# Note that for compatibility with gsctool, the endian of respective 4 bytes are
# reversed.

/usr/share/cros/hwsec-utils/tpm2_read_board_id "$@"
