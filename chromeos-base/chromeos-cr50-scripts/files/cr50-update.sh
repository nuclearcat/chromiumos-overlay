#!/bin/sh
# Copyright 2023 The ChromiumOS Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
#
# This script is run at postinstall phase of Chrome OS installation process.
# It checks if the currently running cr50 image is ready to accept a
# background update and if the resident trunks_send utility is capable of
# updating the H1. If any of the checks fails, the script exits, otherwise it
# tries updating the H1 with the new cr50 image.

/usr/share/cros/hwsec-utils/cr50_update "$@"
