# Copyright 2021 The ChromiumOS Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE.makefile file.

EAPI=7

CROS_WORKON_COMMIT=("a06acfd6fa11e4b41590a9046ae3cd2e22b93bac" "c1488028cf525335b95b277112795219dc6b7688" "c07d08e7f6981cb98d075b78be1e9cd69c76366c" "12b923775846d0bc4b3d77b8cefe9c244e49d8a9" "b3d1fb38e4759e6b36acf245067ff877750c4ba1")
CROS_WORKON_TREE=("992c3c7009bdf03f2282cd3b4d65b4cb097c3523" "5420b48350a5055d087c03d73cf541644e001d2a" "2edb8982bc83966c8d83cfd8e3eff2625017e149" "312779b87ee6f40d9ef20ea0bfdf205042228022" "e50be375f672f5d056234a3c0cdad8f4c3d4eb7f")
CROS_WORKON_USE_VCSID=1
CROS_WORKON_PROJECT=(
	"chromiumos/third_party/zephyr"
	"chromiumos/third_party/zephyr/cmsis"
	"chromiumos/third_party/zephyr/hal_stm32"
	"chromiumos/third_party/zephyr/nanopb"
	"chromiumos/platform/ec"
)

CROS_WORKON_LOCALNAME=(
	"third_party/zephyr/main"
	"third_party/zephyr/cmsis"
	"third_party/zephyr/hal_stm32"
	"third_party/zephyr/nanopb"
	"platform/ec"
)

CROS_WORKON_DESTDIR=(
	"${S}/zephyr-base"
	"${S}/modules/cmsis"
	"${S}/modules/hal_stm32"
	"${S}/modules/nanopb"
	"${S}/modules/ec"
)

inherit cros-workon cros-zephyr-utils

DESCRIPTION="Zephyr based Embedded Controller firmware"
KEYWORDS="*"

src_compile() {
	cros-zephyr-compile zephyr-ec
}

src_install() {
	local firmware_name project
	local root_build_dir="build"

	while read -r firmware_name && read -r project; do
		if [[ -z "${project}" ]]; then
			continue
		fi

		insinto "/firmware/${firmware_name}"
		doins "${root_build_dir}/${project}"/output/*
	done < <(cros_config_host "get-firmware-build-combinations" zephyr-ec || die)
}
