# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

CROS_WORKON_COMMIT="f7de13e7a2c8c82aa92ebd0a490b63979a14b363"
CROS_WORKON_TREE="87ca43c9df43945843d3b5ccc0bc5a48db8be76d"
CROS_WORKON_PROJECT="chromiumos/platform/feature-management"
CROS_WORKON_LOCALNAME="platform/feature-management"

CROS_WORKON_INCREMENTAL_BUILD=1

inherit cros-workon

DESCRIPTION="Public Feature data"
HOMEPAGE="https://chromium.googlesource.com/chromiumos/platform/feature-management"

LICENSE="BSD-Google"
KEYWORDS="*"
IUSE="feature_management feature_management_bsp"

# Only a DEPEND, since this package only install file needed for compiling
# libsegmentation.
DEPEND="
	feature_management? ( chromeos-base/feature-management-private:= )
	feature_management_bsp? ( chromeos-base/feature-management-bsp:= )
"

BDEPEND="
	dev-go/lucicfg
	dev-libs/protobuf
"

src_prepare() {
	if use feature_management; then
		# Install private starlak feature file, if any.
		find "${SYSROOT}/build/share/feature-management/private" -name "*.star" \
				-exec cp -t "${S}" {} \+ || die
		# Install device selection file if present.
		find "${SYSROOT}/build/share/feature-management/private" \
				-name "device_selection.textproto" \
				-exec cp -t "${S}/devices" {} \+ || die
	fi
	default
}

src_compile() {
	emake V=1
}

src_install() {
	insinto "/usr/include/libsegmentation"
	doins "${S}/generated/libsegmentation_pb.h"
	insinto "/build/share/libsegmentation"
	doins -r "${S}/proto"
}
