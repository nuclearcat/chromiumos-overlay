# Copyright 2023 The ChromiumOS Authors
# Distributed under the terms of the GNU General Public License v2

EAPI="7"

DESCRIPTION="Log collection utility for Quectel modems"
HOMEPAGE="https://github.com/quectel-official/QLog"
GIT_SHA1="a04ecaa3bb6b58d970690f86bd5cb556819a59db"
SRC_URI="${HOMEPAGE}/archive/${GIT_SHA1}.tar.gz -> QLog-${GIT_SHA1}.tar.gz"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="*"
IUSE=""

RDEPEND=""
DEPEND="${RDEPEND}"

S="${WORKDIR}/QLog-${GIT_SHA1}"
