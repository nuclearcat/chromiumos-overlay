# Copyright 2012 The ChromiumOS Authors
# Distributed under the terms of the GNU General Public License v2

EAPI="7"

# Track all numbered kernel repos.
# This array is static due to tooling limitations. Specically, inheriting
# cros-kernel-versions doesn't consistently work in all of the ways that this
# ebuild is processed.
CROS_WORKON_COMMIT=("bdd898e5832323eecd337ad8cf8caade71a4cc01" "97bf2ac990f274a26587076305c2d8889e5744de" "b08da8b063eb27c5abf422cb920371009527d2b5" "70f88e1096914094f7875352f0fc5fd2b741191d" "40beabb4702f4b85beb87f84ec85ba1c1e8b2d46" "3fe19b0f978e3e7c62626cf22eed2c961ed775e2" "be0220761539a9a15e8890bfc52192b913a7e6c0")
CROS_WORKON_TREE=("907939eb6942aea1a472a621125515a4cff92e41" "1fe9d9a723d5e50f7fdf5e24e08ec6db8aaab3b7" "16e5751e3cb02d3d674d37aa75fb7559635854cc" "4bdffadd7a330a9f7d3451e35d4b1b9e1b0a1d25" "4a114a3497891496b8bfd926657502bef11b49e4" "3170cbedcebd7f89c9a4e29f0ba5dcec79756822" "06a7de9f0f3465e7daf9b661ba824c4108ed362a")
CROS_WORKON_PROJECT=(
	"chromiumos/third_party/kernel"
	"chromiumos/third_party/kernel"
	"chromiumos/third_party/kernel"
	"chromiumos/third_party/kernel"
	"chromiumos/third_party/kernel"
	"chromiumos/third_party/kernel"
	"chromiumos/third_party/kernel"
)
CROS_WORKON_LOCALNAME=(
	"kernel/v4.4"
	"kernel/v4.14"
	"kernel/v4.19"
	"kernel/v5.4"
	"kernel/v5.10"
	"kernel/v5.15"
	"kernel/v6.1"
)

inherit cros-workon cros-kernel-versions

DESCRIPTION="Chrome OS Kernel virtual package"
HOMEPAGE="http://src.chromium.org"

LICENSE="metapackage"
KEYWORDS="*"
S="${WORKDIR}"

# Check if the static arrays defined above need to be updated to reflect a
# change to cros-kernel-versions' CHROMEOS_KERNELS.
assert_localname_sync() {
	local k v actual expected expected_localname=()
	# shellcheck disable=SC2154
	for k in "${CHROMEOS_KERNELS[@]}"; do
		if [[ "${k}" =~ chromeos-kernel-([0-9]+_[0-9]+) ]]; then
			v="${BASH_REMATCH[1]//_/.}"
			expected="kernel/v${v}"
			expected_localname+=("${expected}")
			if [[ ! "${CROS_WORKON_LOCALNAME[*]}" =~ ${expected} ]]; then
				die "Append ${expected} to ${CATEGORY}/${PN} CROS_WORKON_LOCALNAME"
			fi
		fi
	done
	for actual in "${CROS_WORKON_LOCALNAME[@]}"; do
		if [[ ! "${expected_localname[*]}" =~ ${actual} ]]; then
			die "Remove ${actual} from ${CATEGORY}/${PN} CROS_WORKON_LOCALNAME"
		fi
	done
}
assert_localname_sync

# shellcheck disable=SC2154
IUSE="${!CHROMEOS_KERNELS[*]}"
# exactly one of foo, bar, or baz must be set, but not several
REQUIRED_USE="^^ ( ${!CHROMEOS_KERNELS[*]} )"

# shellcheck disable=SC2154
RDEPEND="
	$(for v in "${!CHROMEOS_KERNELS[@]}"; do echo  "${v}? (  sys-kernel/${CHROMEOS_KERNELS[${v}]} )"; done)
"

# Add blockers so when migrating between USE flags, the old version gets
# unmerged automatically.
# shellcheck disable=SC2154
RDEPEND+="
	$(for v in "${!CHROMEOS_KERNELS[@]}"; do echo "!${v}? ( !sys-kernel/${CHROMEOS_KERNELS[${v}]} )"; done)
"

# Default to the latest kernel if none has been selected.
# TODO: This defaulting does not work. Fix or remove.
RDEPEND_DEFAULT="sys-kernel/chromeos-kernel-5_4"
# Here be dragons!
RDEPEND+="
	$(printf '!%s? ( ' "${!CHROMEOS_KERNELS[@]}")
	${RDEPEND_DEFAULT}
	$(printf '%0.s) ' "${!CHROMEOS_KERNELS[@]}")
"

src_unpack() { :; }
